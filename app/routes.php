<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(array('before' => 'guest'), function()
{
    Route::get('/', 'AuthController@index');
    Route::get('/login', 'AuthController@index');
    Route::post('login', 'AuthController@login');
});

Route::group(array('before' => 'auth'), function()
{
    Route::get('/', 'CategoriesController@index');
    Route::get('logout', 'AuthController@logout');
    Route::get('categories', 'CategoriesController@index');
    Route::get('machine-managment/{category_id}', 'MachinesController@index');
    Route::get('machine-managment/{category_id}/new', 'MachinesController@showAdd');
    Route::get('machine-managment-lceformfileds', 'MachinesController@lceformfileds');
    Route::post('add-machine', 'MachinesController@doAdd');
    Route::get('machine-managment/{category_id}/edit/{machine_id}', 'MachinesController@showEdit');
    Route::post('edit-machine', 'MachinesController@doEdit');
    Route::post('update-place', 'MachinesController@updatePlace');
    Route::get('delete/{machine_id}', 'MachinesController@delete');
    Route::get('delete-pdf/{machine}/{language}', 'MachinesController@deletePdf');
    Route::get('delete-video/{machine}/{language}/{video}', 'MachinesController@deleteVideo');
    Route::post('create-thumbnail', 'ImagesController@createThumbnail');
    Route::post('remove-image', 'ImagesController@removeImage');

});

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::get('languages', 'LanguageController@getAllLanguages');
    Route::get('languages/{language_id}', 'LanguageController@getLanguages');
    Route::get('categories', 'CategoriesController@getAllCategories');
    Route::get('machines/{machine_id}', 'MachinesController@getMachines');
//    Route::get('machine-history/{machine_id}', 'HistoryController@getMachineHistory');
    Route::resource('device-token', 'DeviceTokensController');
});