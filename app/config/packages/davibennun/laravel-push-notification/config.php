<?php

return array(

    'hitachi'=>array(
        'environment' => 'development',
        'certificate' => base_path().'/ck.pem',
        'passPhrase' => 'vivify',
        'service' => 'apns'
    ),

    'hitachi_prod' => array(
        'environment' => 'production',
        'certificate' => base_path().'/ck_prod.pem',
        'passPhrase' => 'HitachiCatalogue',
        'service' => 'apns'
    )
);
