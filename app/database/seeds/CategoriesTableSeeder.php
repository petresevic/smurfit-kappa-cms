<?php

class CategoriesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('categories')->delete();

        $categories = array(
            array('title' => 'Excavators'),
            array('title' => 'Mini', 'parent_id' => 1),
            array('title' => 'Medium', 'parent_id' => 1),
            array('title' => 'Large', 'parent_id' => 1),
            array('title' => 'Wheeled', 'parent_id' => 1),
            array('title' => 'WheelLoaders'),
            array('title' => 'Cranes'),
            array('title' => 'Rigid dump trucks'),
            array('title' => 'Application'),
            array('title' => 'Demolition', 'parent_id' => 9),
            array('title' => 'Clamshelltelescopicarm', 'parent_id' => 9),
            array('title' => 'Superlongfront', 'parent_id' => 9),
            array('title' => 'Forestry', 'parent_id' => 9),
        );

        foreach ($categories as $category) {
            $category['slug'] = Str::slug($category['title']);
            Category::create($category);
        }
    }
}