<?php

class LanguagesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('languages')->delete();

        $languages = array(
            array('name' => 'Dansk'),
            array('name' => 'Deutsch'),
            array('name' => 'English'),
            array('name' => 'Espanol'),
            array('name' => 'Francais'),
            array('name' => 'Italiano'),
            array('name' => 'Nederlands'),
            array('name' => 'Norsk'),
            array('name' => 'Polski', 'required' => false),
            array('name' => 'Portugues'),
            array('name' => 'Soumalainen','required' => false),
            array('name' => 'Svenska'),
            );

        foreach ($languages as $language) {
            $language['slug'] = Str::slug($language['name']);
            Language::create($language);
        }
    }
}