<?php

class DeviceTokensTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('device_tokens')->delete();

        $deviceTokens = array(
            array('token' => '4a461429ec9b85745a189ae336c3029d30754a23c605d4eef099b52086c9fac0'),
            array('token' => '8673a21365db3a3701a10a0d255793f52995a6a628cb8b4b6e84592e5d3e595c'),
            array('token' => '8e854676ec3048bc7d4d231191731bd853be60a1ab885eadaaaf6fc5f3d5b349')
        );

        foreach ($deviceTokens as $token) {
            DeviceToken::create($token);
        }
    }
}