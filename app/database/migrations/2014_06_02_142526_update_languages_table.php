<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Language::where('slug', '=', 'espanol')->update(array('name' => 'Español'));
        Language::where('slug', '=', 'francais')->update(array('name' => 'Français'));
        Language::where('slug', '=', 'portugues')->update(array('name' => 'Português'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Language::where('slug', '=', 'espanol')->update(array('name' => 'Espanol'));
        Language::where('slug', '=', 'francais')->update(array('name' => 'Francais'));
        Language::where('slug', '=', 'portugues')->update(array('name' => 'Portugues'));
	}

}
