<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('machine_languages'))
        {
            Schema::create('machine_languages', function($table)
            {
                $table->increments('id');
                $table->integer('machine_id');
                $table->integer('language_id');
                $table->string('model', 100);
                $table->string('engine_power', 60);
                $table->string('weight', 60);
                $table->string('backhoe_bucket', 60);
                $table->string('note', 200);
                $table->text('introduction_text');
                $table->timestamps();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('machine_languages');
	}

}
