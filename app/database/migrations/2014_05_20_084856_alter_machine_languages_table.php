<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMachineLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('machine_languages', function($table) {
            $table->string('max_digging_dept')->after('backhoe_bucket');
            $table->string('bucket_digging_force')->after('max_digging_dept');
            $table->string('max_lift_capacity')->after('bucket_digging_force');
            $table->string('max_boom_length')->after('max_lift_capacity');
            $table->string('max_boom_fly_jib')->after('max_boom_length');
            $table->string('max_line_pull')->after('max_boom_fly_jib');
            $table->string('max_travel_speed')->after('max_line_pull');
            $table->string('max_payload')->after('max_travel_speed');
            $table->string('body_capacity_heaped')->after('max_payload');
            $table->string('breakout_force')->after('body_capacity_heaped');
            $table->string('static_tipping_load')->after('breakout_force');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('machine_languages', function($table){
            $table->dropColumn('max_digging_dept');
            $table->dropColumn('bucket_digging_force');
            $table->dropColumn('max_lift_capacity');
            $table->dropColumn('max_boom_length');
            $table->dropColumn('max_boom_fly_jib');
            $table->dropColumn('max_line_pull');
            $table->dropColumn('max_travel_speed');
            $table->dropColumn('max_payload');
            $table->dropColumn('body_capacity_heaped');
            $table->dropColumn('breakout_force');
            $table->dropColumn('static_tipping_load');
        });
	}
}