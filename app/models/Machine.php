<?php

class Machine extends Eloquent {

    protected static $photoRules = array(
        'main_picture' => 'required',
        'pictures' => 'requireOnePhoto:0,0'
    );

    protected static $videoRules = array(
        'video' => 'video|max:10000',
    );

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'machines';

    /**
     * Mass fillable fields
     *
     * @var array
     */
    protected $fillable = array(
        'category_id', 'place'
    );

    protected $hidden = array('created_at',
                              'updated_at',
                              'place');

    public function machineLanguages()
    {
        return $this->hasMany('MachineLanguages' , 'machine_id');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public static function getPhotoRules()
    {
        return self::$photoRules;
    }

    public static function getVideoRules()
    {
        return self::$videoRules;
    }

    
}