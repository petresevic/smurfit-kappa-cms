<?php

class Category extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $hidden = array('created_at', 'updated_at');

    protected $guarded = array();

    public function subCategories()
    {
        return $this->hasMany('Category' , 'parent_id');
    }

    public function masterCategory()
    {
        return $this->belongsTo('Category' , 'parent_id');
    }

    public function machines()
    {
        return $this->hasMany('Machine');
    }

}
