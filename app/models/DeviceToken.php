<?php

class DeviceToken extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device_tokens';

    /**
     * Mass fillable fields
     *
     * @var array
     */
    protected $fillable = array(
        'token'
    );

}
