<?php

class History extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'history';

    /**
     * Mass fillable fields
     *
     * @var array
     */
    protected $fillable = array(
        'machine_id', 'machine_object'
    );

    public function machine()
    {
        return $this->belongsTo('Machine');
    }

}