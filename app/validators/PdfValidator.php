<?php

class PdfValidator
{
    public function pdf($field, $value, $params)
    {
        if (isset($value) && $value->getMimeType() == 'application/pdf') {
            return true;
        }
        
        return false;
    }
}
