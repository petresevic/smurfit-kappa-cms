<?php

class PhotoValidator
{
    public function requireOnePhoto($field, $value, $params)
    {
        foreach ($value as $key => $val) {
            if ($val != null) {
                if($this->resolution($field, $val, $params) ) {
                    return true;
                }
            }
        }
        return true;
    }

    public function resolution($field, $value, $params)
    {
        $size = getimagesize($value->getRealPath());

        if ($size[0] >= $params[0] && $size[1] >= $params[1]) {
            return true;
        } else {
            return false;
        }
    }
}
