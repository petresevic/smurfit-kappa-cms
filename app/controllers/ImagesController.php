<?php

class ImagesController extends BaseController {

    public function createThumbnail()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];
        $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;

        $img = false;
        if (file_exists($imagePath)) {
            $img = Image::make($imagePath);
        }
        
        if ($img) {
            $img->crop(
                intval($inputs['w']),
                intval($inputs['h']),
                intval($inputs['x']),
                intval($inputs['y'])
            );
            
            $pos = strpos($imageName, 'photo');
            if ($pos !== false) {
                $img->fit(627, 940);
            } else {
                $img->fit(460, 620);
            }
                        
            $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
            $img->save(public_path().'/machines/'.$machineId.'/'.$thumbnailName);
            $this->setMessage('Thumbnail created successfully.');
        } else {
            $this->setErrorMessage('There was error');
        }
    }

    public function removeImage()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];

        $deleted = false;
        if ($this->isThumbnail($imageName)) {
            $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;
            $deleted = $this->deleteImageFile($imagePath);
        } else {
            if (!$this->isLastImage($machineId)) {
                $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;
                $deleted = $this->deleteImageFile($imagePath);
                $this->removeThumbnail($machineId, $imageName);
            } else {
                $this->setErrorMessage('You can\'t delete all photos.');
                return json_encode(array('message' => 'error'));
            }
        }

        if ($deleted) {
            $this->setErrorMessage('Successfully deleted.');
            return json_encode(array('message' => 'ok'));
        } else {
            $this->setErrorMessage('There was error.');
            return json_encode(array('message' => 'error'));
        }
    }

    private function isThumbnail($imageName)
    {
        return strpos($imageName,'thumbnail') !== false;
    }

    private function isLastImage($machineId)
    {
        $numberOfImages = 0;
        for ($i=1; $i < 11; $i++) {
            $imagePath = public_path().'/machines/'.$machineId.'/'.'photo_'.$i.'.jpg';
            if (file_exists($imagePath)) {
                $numberOfImages++;
            }
        }
        return $numberOfImages < 2;
    }

    private function removeThumbnail($machineId, $imageName)
    {
        $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
        $thumbnailPath = public_path().'/machines/'.$machineId.'/'.$thumbnailName;
        $this->deleteImageFile($thumbnailPath);
    }

    private function deleteImageFile($imagePath)
    {
        if (file_exists($imagePath)) {
            unlink($imagePath);
            return true;
        }
        return false;
    }
}