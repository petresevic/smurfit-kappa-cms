<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Set flash message
	 *
	 * @param string $message
	 * @param string $type
	 */
	protected function setMessage($message, $type = 'alert-success')
	{
		Session::flash('message', $message);
		Session::flash('message_type', $type);
	}

	/**
	 * Set flash error message
	 *
	 * @param string $message
	 */
	protected function setErrorMessage($message)
	{
		$this->setMessage($message, 'alert-danger');
	}

}