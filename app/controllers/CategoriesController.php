<?php

class CategoriesController extends BaseController {

    public $categoriesService;

    public function __construct(CategoriesService $categoriesService)
    {
        $this->categoriesService = $categoriesService;
    }

    public function index()
    {
        $categories = $this->categoriesService->getAllCategoriesWithSubcategories();
        $breadcrumbsData = array('pages' => array('categories' => 'Categories'));

            return View::make('categories', array('categories' => $categories))
                   ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function getAllCategories()
    {
        return Category::all();
    }
}