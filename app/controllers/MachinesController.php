<?php

class MachinesController extends BaseController
{

    public $machinesService;
    public $pushRequestService;
    public $historyService;

    public function __construct(MachinesService $machinesService, PushRequestService $pushRequestService, HistoryService $historyService)
    {
        $this->machinesService = $machinesService;
        $this->pushRequestService = $pushRequestService;
        $this->historyService = $historyService;
    }

    public function index($category_id)
    {
        $englishLanguageId = Language::where('slug', '=', 'english')->first()->id;

        $machines = Machine::with(array('machineLanguages' => function($query) use ($englishLanguageId) {
                        $query->where('machine_languages.language_id', '=', $englishLanguageId);
                    }))
                ->where('category_id', '=', $category_id)
                ->orderBy('place', 'asc')
                ->get();
        $category = Category::find($category_id);

        $breadcrumbsData = array('pages' => array('categories' => 'Categories',
                'machine-managment/' . $category_id => Category::find($category_id)->title . ' management'));

        return View::make('machines.index', array('machines' => $machines, 'category_id' => $category_id, 'category' => $category))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function showAdd($category_id)
    {
        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');


        $category = Category::find($category_id);

        $categoryTitle = Category::find($category_id)->title;




        $breadcrumbsData = array('pages' => array('categories' => 'Categories',
                'machine-managment/' . $category_id => $categoryTitle . '  management',
                'machine-managment/' . $category_id . '/new' => $categoryTitle . ' new'));

        return View::make('machines.add', array('languages' => $languages, 'category_id' => $category_id, 'category' => $category, 'requiredLanguageIds' => $requiredLanguageIds, 'randNumber' => time()))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function doAdd()
    {
        try {
            $machine = $this->machinesService->store(Input::all());

            $this->setMessage('New machine created successfully.');
            $this->pushRequestService->push($machine->id);
            $this->historyService->store($machine->id, $this->getMachines($machine->id)->toJson());
            return Redirect::to('/machine-managment/' . Input::input('category_id'));
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('main_picture', 'pictures'));
        }
    }

    public function showEdit($category_id, $machine_id)
    {
        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');

        // get all languages with machine languages id of $machine_id
        $languages = Language::with(array('machineLanguages' => function($query) use ($machine_id) {
                        $query->where('machine_languages.machine_id', '=', $machine_id);
                    }, 'machineLanguages.machine.category'))->get();

        $data = array();
        foreach ($languages->toArray() as $language) {
            $data[$language['id']] = $language['machine_languages'][0];
        }

        $categoryTitle = Category::find($category_id)->title;

        $category = Category::find($category_id);


        $breadcrumbsData = array('pages' => array('categories' => 'Categories',
                'machine-managment/' . $category_id => $categoryTitle . ' management',
                'machine-managment/' . $category_id . '/edit/' . $machine_id => $categoryTitle . ' edit'));

        return View::make('machines.edit', array('languages' => $languages,
                            'category_id' => $category_id,
                            'category' => $category,
                            'machine_id' => $machine_id,
                            'data' => $data,
                            'requiredLanguageIds' => $requiredLanguageIds))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function doEdit()
    {
        

        try {
            $oldMachine = $this->getMachines(Input::get('machine_id'))->toJson();
            $machine = $this->machinesService->update(Input::all());
            $this->setMessage('Machine updated successfully.');
            if ($machine['machineUpdate'] == true) {
                $this->pushRequestService->push(Input::get('machine_id'));
//                $this->historyService->update(Input::get('machine_id'), $oldMachine);
            } else if (count($machine['languageUpdate']) > 0) {
//                //$this->pushRequestService->push(Input::get('machine_id'), $machine['languageUpdate']);
//                //$this->historyService->update(Input::get('machine_id'), $oldMachine);
            }
            return Redirect::to('/machine-managment/' . Input::input('category_id'));
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('main_picture', 'pictures'));
        }
    }

    public function updatePlace()
    {
        foreach (Input::get('ids') as $index => $id) {
            Machine::find($id)->update(array('place' => $index + 1));
        }
    }

    public function delete($machine_id)
    {
        // delete machine
        $machine = Machine::find($machine_id)->delete();

        // delete machine history
        $machine = History::where('machine_id', '=', $machine_id)->delete();

        // delete machine languages
        $machineLanguages = MachineLanguages::where('machine_id', '=', $machine_id)->delete();

        // delete files
        $this->machinesService->deleteFiles($machine_id);

        $this->setMessage('Machine deleted successfully.');
        return Redirect::back();
    }

    public function getMachines($machineId)
    {
        $query = Machine::where('id', $machineId);

        $lang = Input::get('lang');

        if (!empty($lang)) {
            $langs = explode(',', $lang);
            $query->with(array('MachineLanguages' => function($q) use ($langs) {
                    $q->join('languages', 'language_id', '=', 'languages.id')
                            ->whereIN('machine_languages.language_id', $langs)
                            ->orWhereIN('languages.slug', $langs);
                }));
        } else {
            $query->with('MachineLanguages');
        }

        return $results = $query->get();
    }

    public function deletePdf($machine, $language)
    {
        $isFileDeleted = $this->machinesService->deletePdf($machine, $language);

        return json_encode(array('deleted' => $isFileDeleted));
    }

    public function deleteVideo($machine, $language, $video)
    {
        $isFileDeleted = $this->machinesService->deleteVideo($machine, $language, $video);

        return json_encode(array('deleted' => $isFileDeleted));
    }

    public function lceformfileds()
    {

        return View::make('machines.lceformfileds', array('randNumber' => time()));
    }

}
