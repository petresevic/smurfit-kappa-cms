<?php

class AuthController extends BaseController {

    public function index()
    {
        return View::make('login');
    }

    public function login()
    {
        $rules = array(
            'username'    => 'required|alphaNum',
            'password' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back
        if ($validator->fails()) {
            return Redirect::to('/login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $userdata = array(
                'username'     => Input::get('username'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // validation successful
                return Redirect::to('categories');
            } else {
                // validation not successful, redirect back
                return Redirect::to('/login')
                    ->withErrors(array('username' => 'Invalid username/password .'))
                    ->withInput(Input::except('password'));
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}
