<?php

class CategoriesService
{
    public function getAllCategoriesWithSubcategories()
    {
        return Category::with('masterCategory', 'subCategories')->get();
    }
}