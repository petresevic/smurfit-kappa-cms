<?php

class PushRequestService
{
    public function push($machineId, $languageIds = array())
    {
        $tokens = DeviceToken::all()->lists('token');
        $deviceArray = array();
        $app = 'hitachi';
        
        foreach($tokens as $token) {
            $deviceArray[] = PushNotification::Device($token);
        }
        
        
        
        if (count($deviceArray) != 0) {
            $devices = PushNotification::DeviceCollection($deviceArray);
            $message = $this->makeMessage($machineId, $languageIds);
                     
            
            
            if (App::environment() == 'production') {
                $app = 'hitachi_prod';
            }

            PushNotification::app($app)
                    ->to($devices)
                    ->send($message);
        }
    }

    private function makeMessage($machineId, $languageIds)
    {
        $updated = json_encode(
            array(
                'id' => $machineId,
                'langs' => $languageIds
            )
        );

        return PushNotification::Message('Hitachi update!', array(
            'badge' => 1,
            'custom' => array('updated' => $updated)
        ));
    }
}
