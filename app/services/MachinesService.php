<?php

class MachinesService
{

    public function store($inputs)
    {


        $this->validateInputs(
                $inputs, Machine::getPhotoRules(), Machine::getVideoRules()
        );

        $place = Machine::where('category_id', '=', $inputs['category_id'])->max('place');

        if ($place == null) {
            $place = 1;
        } else {
            $place += 1;
        }

        // create new machine
        $machine = Machine::create(array('category_id' => $inputs['category_id'],
                    'place'       => $place));


        $this->saveFiles(
                $machine->id, $inputs['main_picture'], $inputs['pictures']
        );



        // add machine languages
        foreach ($inputs as $index => $input) {
            if (substr($index, 0, 5) == "lang_") {

                $input['machine_id'] = $machine->id;

                $i = 1;
                foreach ($input['videos'] as $video) {
                    if (isset($video)) {
                        $video->move('machines/' . $machine->id, Language::find($input['language_id'])->slug . '_video_' . $i . '.mp4');
                    }
                    $i++;
                }


                MachineLanguages::create($input);
            }
        }

        return $machine;
    }

    private function validateInputs($inputs, $photoRules, $videoRules)
    {
        $validators = array();

        // validate photos
        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        $messages = array(
            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution'        => 'Resolution is not right.'
        );

        Validator::extend('video', 'VideoValidator@video');
        $videoValidatorMessages = array(
            'video' => 'Please upload mp4 video file.',
        );


        $photoValidator = Validator::make(
                        $inputs, $photoRules, $messages
        );

        if ($photoValidator->fails()) {
            $validators['photo'] = $photoValidator;
        }

        $requiredLanguages = Language::getRequiredLanguagesIds();

        foreach ($inputs as $index => $input) {

            // language inputs have multiple fields
            if (substr($index, 0, 5) == "lang_") {

               
                // validate videos
                for ($i = 1; $i < 6; $i++) {
                    if (isset($input['videos'][$i]) && isset($input['videos'][$i]) != '') {
                        $videoInput     = array('video' => $input['videos'][$i]);
                        $videoValidator = Validator::make(
                                        $videoInput, $videoRules, $videoValidatorMessages
                        );

                        if ($videoValidator->fails()) {
                            $validators['lang_' . $input['language_id'] . '_video_' . $i] = $videoValidator;
                        }
                    }
                }

                $inputText   = array(
                    'name'     => $input['name'],
                    'designer'=> $input['designer'],
                    'country' => $input['country'],
                    'intro'   => $input['intro'],
                );
                
                $rules = array(
                    'name'     => 'required',
                    'designer'=> 'required',
                    'country' => 'required',
                    'intro'   => 'required',
                );
                
                $inputTextValidator = Validator::make($inputText, $rules);

                if ($inputTextValidator->fails()) {
                    $validators['lang_' . $input['language_id'] . '[name]' . $i]     = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[designer]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[country]' . $i]  = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[intro]' . $i]    = $inputTextValidator;
                }


                //validate inputs
            }
        }

        if (count($validators) > 0) {
            throw new ValidatorException($validators);
        }
    }

    public function update($inputs)
    {
        $photoRules = Machine::getPhotoRules();
        $videoRules = Machine::getVideoRules();



        $machineUpdate  = false;
        $languageUpdate = array();

        // if main picture is not provided skip validation
        if (!isset($inputs['main_picture'])) {
            unset($photoRules['main_picture']);
        } else {
            $machineUpdate = true;
        }



        // if pictures is not provided skip validation
        if (isset($inputs['pictures'])) {
            $countPictures = 0;
            foreach ($inputs['pictures'] as $value) {
                if ($value === null) {
                    $countPictures += 1;
                }
            }

            if ($countPictures == 10) {
                unset($photoRules['pictures']);
            } else {
                $machineUpdate = true;
            }

            foreach ($inputs['pictures'] as $index => $value) {
                if (isset($inputs['machine_id']) && $value === '' && $countPictures > 1) {
                    unset($photoRules['pictures']);
                    $dirPath     = public_path() . '/machines/' . $inputs['machine_id'];
                    $picturePath = $dirPath . '/photo_' . $index . '.jpg';
                    if (file_exists($picturePath)) {
                        unlink($picturePath);
                    }
                    $pictureThumbnailPath = $dirPath . '/photo_' . $index . '_thumbnail.jpg';
                    if (file_exists($pictureThumbnailPath)) {
                        unlink($pictureThumbnailPath);
                    }
                    $machineUpdate = true;
                }
            }
        } else {
            unset($photoRules['pictures']);
        }

        // validate inputs
        $this->validateInputs($inputs, $photoRules, $videoRules);


        $machineId = $inputs['machine_id'];

        $machineLanguages = MachineLanguages::where('machine_id', '=', $machineId)->get();

        $this->saveFiles(
                $machineId, $inputs['main_picture'], $inputs['pictures']
        );



        // update specs values
        foreach ($machineLanguages as $machineLanguage) {

            $index = 'lang_' . $machineLanguage->language_id;

            $specs = $inputs[$index];

            unset($specs['videos']);

            $oldAttributes = $machineLanguage->getAttributes();
            unset($oldAttributes['id']);
            unset($oldAttributes['machine_id']);
            unset($oldAttributes['created_at']);
            unset($oldAttributes['updated_at']);


            $machineLanguage->update($specs);

            $i = 1;



            foreach ($inputs[$index]['videos'] as $video) {
                if (isset($video) && $video != '') {
                    $video->move('machines/' . $machineId, Language::find($machineLanguage->language_id)->slug . '_video_' . $i . '.mp4');
                }

                $i++;
            }



            foreach ($oldAttributes as $key => $attr) {

                if (isset($key) && isset($specs[$key])) {

                    if ($attr !== $specs[$key]) {
                        $languageUpdate[] = $machineLanguage->language_id;
                    }
                }
            }
        }

        return array(
            'machineUpdate'  => $machineUpdate,
            'languageUpdate' => $languageUpdate
        );
    }

    public function saveFiles($machineId, $mainPicture, $pictures)
    {
        // save main image
        if (isset($mainPicture)) {

            $mainPicture->move('machines/' . $machineId, 'main_picture.jpg');
            $img = Image::make('machines/' . $machineId . '/main_picture.jpg');

            if ($img->height() >= $img->width()) {

                $img->fit(815, 1222);
            } else {
                $img->fit(1222, 815);
            }
            $img->save('machines/' . $machineId . '/main_picture.jpg');

            $thumbnailImg = Image::make('machines/' . $machineId . '/main_picture.jpg');
            $thumbnailImg->fit(460, 620);
            $thumbnailImg->save(public_path() . '/machines/' . $machineId . '/main_picture_thumbnail.jpg');
        }


        // save photos
        $i = 1;
        foreach ($pictures as $index => $picture) {
            if ($picture != null) {
                $picture->move('machines/' . $machineId, 'photo_' . $i . '.jpg');

                $img = Image::make('machines/' . $machineId . '/photo_' . $i . '.jpg');
                if ($img->height() >= $img->width()) {

                    $img->fit(815, 1222);
                } else {

                    $img->fit(1222, 815);
                }
                $img->save('machines/' . $machineId . '/photo_' . $i . '.jpg');

                $thumbnailImg = Image::make('machines/' . $machineId . '/photo_' . $i . '.jpg');
                $thumbnailImg->fit(627, 940);
                $thumbnailImg->save(public_path() . '/machines/' . $machineId . '/photo_' . $i . '_thumbnail.jpg');
            }
            $i += 1;
        }
    }

    public function deleteFiles($machine_id)
    {
        $dirPath = public_path() . '/machines/' . $machine_id;

        // delete main picture
        $mainPicturePath = $dirPath . '/main_picture.jpg';
        if (file_exists($mainPicturePath)) {
            unlink($mainPicturePath);
        }
        
        
        $mainPictureThumbnailPath = $dirPath . '/main_picture_thumbnail.jpg';
        if (file_exists($mainPictureThumbnailPath)) {
            unlink($mainPictureThumbnailPath);
        }
        
        
        // delete photos
        for ($i = 1; $i < 11; $i++) {
            $picturePath = $dirPath . '/photo_' . $i . '.jpg';
            if (file_exists($picturePath)) {
                unlink($picturePath);
            }
            $pictureThumbnailPath = $dirPath . '/photo_' . $i . '_thumbnail.jpg';
            if (file_exists($pictureThumbnailPath)) {
                unlink($pictureThumbnailPath);
            }
        }


        // delete directory
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }
    }

    public function deleteVideo($machine, $language, $video)
    {
        $dirPath = public_path() . '/machines/' . $machine;

        $videoPath = $dirPath . '/' . $language . '_video_' . $video . '.mp4';
        if (file_exists($videoPath)) {
            unlink($videoPath);
        }

        return file_exists($videoPath) ? false : true;
    }

    protected function formatOptions($machineOptions, $machineOptionLabel)
    {
        
    }

}
