<div class="input-field-group">
    <div class="col-lg-3">
      <label class="control-label pull-right" for="lang_{{$languageId}}[name]">Entity</label>
    </div>
    <div class="col-lg-6">
      <input name="lang_{{$languageId}}[name]" value="{{$input['name'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_name" >
    </div>
    <div class="error_lang_{{$languageId}}_name error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
      <label class="control-label pull-right" for="lang_{{$languageId}}[designer]">Inventor</label>
    </div>
    <div class="col-lg-6">
      <input name="lang_{{$languageId}}[designer]" value="{{$input['designer'] or ''}}" type="text" class="form-control input-md"  id="lang_{{$languageId}}_designer">
    </div>
    <div class="error_lang_{{$languageId}}_designer error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
      <label class="control-label pull-right" for="lang_{{$languageId}}[country]">Country</label>
    </div>
    <div class="col-lg-6">
      <input name="lang_{{$languageId}}[country]" value="{{$input['country'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_country">
    </div>
    <div class="error_lang_{{$languageId}}_country error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[intro]">Intro</label>
    </div>
    <div class="col-lg-6">
        <textarea name="lang_{{$languageId}}[intro]" class="form-control input-md" rows="5" id="lang_{{$languageId}}_intro">{{$input['intro'] or ''}}</textarea>
    </div>
    <div class="error_lang_{{$languageId}}_intro error col-lg-3"></div>
</div>
