<div class="input-field-group pdf-input">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[note]">Pdf @if (in_array($languageId,$requiredLanguageIds)) * @endif</label>
    </div>
    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
        <div class="input-group">
            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
            <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select pdf</span><span class="fileinput-exists">Change</span><input type="file" name="lang_{{$languageId}}[pdf]"></span>
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
    @if ( isset($machine_id) && file_exists(public_path().'/machines/'.$machine_id.'/'.$language->slug.'.pdf'))
        <div class="pdf-info col-lg-3">
            Current pdf: <a href="{{asset('/machines/'.$machine_id.'/'.$language->slug.'.pdf')}}">download</a> @if (!in_array($languageId,$requiredLanguageIds)) | <a class="remove-pdf" href="#" data-machine="{{$machine_id}}" data-language="{{$language->slug}}">remove</a> @endif
        </div>
    @elseif (isset($messages['pdf'][0]))
        <div class="error col-lg-3">{{ $messages['pdf'][0] }}</div>
    @else
        <div class="error col-lg-3"></div>
    @endif
</div>