@extends('layouts/main')

@section('content')

{{$breadcrumbs}}

<h1>{{$category->title}} product management</h1>
<p>
    <a href="/machine-managment/{{$category_id}}/new" class="btn btn-success">Add</a>
</p>
<table class="table table-bordered">
    <thead>
        @if($category->slug == 'lce')
        <tr>
            <th>&nbsp;</th>
            <th width="75%"></th>
            <th width="15%">Controls</th>
        </tr>
        @else
        <tr>
            <th>Model</th>
            <th>Design name</th>
            <th>Country</th>
            <th>Controls</th>
        </tr>
        @endif
    </thead>
    <tbody id="sortable">
        @foreach ($machines as $machine)
         
         <tr class="machine-table-row" data-id="{{$machine->id}}">
            <?php $spec = $machine->machineLanguages->first(); ?>
            <?php if ($spec): ?>
                <td class="handle"><span class="move-icon glyphicon glyphicon-resize-vertical"></span></td>
                <td>{{$spec->name}}</td>
                <td>{{$spec->country}}</td>
            <?php endif; ?>
            
            <td width="15%">
                <a href="/machine-managment/{{$category_id}}/edit/{{$machine->id}}" class="btn btn-primary">Adjust</a>
                <a href="/delete/{{$machine->id}}" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this machine?')">Remove</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>
@stop