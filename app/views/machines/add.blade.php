@extends('layouts/main')

@section('content')

{{$breadcrumbs}}

<h1>{{Category::find($category_id)->title}} new</h1>
<form id="add-machine" action="/add-machine" method="post" enctype="multipart/form-data" class="navbar-form navbar-left">


    <?php
    $messages = json_decode($errors, true);
    isset($messages['photo']) ? $photoErrors = $messages['photo'] : $photoErrors = array();
    ?>
    <div style="margin-right: 50px" class="fileinput fileinput-new" data-provides="fileinput">
        <p class="error">
            {{$photoErrors['main_picture'][0] or null}}
        </p>
        <div><label for="main_picture">Main picture* (min 458x621)</label></div>
        <div class="fileinput-new thumbnail" style="width: 117px; height: 150px;">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 117px; max-height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="main_picture"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>

    <hr>
    <h2>Photo page</h2>
    <p>One photo with minimal resolution of 458x621px is required</p>
    <p class="error photos-error">
        {{$photoErrors['pictures'][0] or null}}
    </p>
    @for ($i=1; $i < 11; $i++)
    <div class="fileinput fileinput-new photos" data-provides="fileinput">
        <div><label for="header_picture">Photo {{$i}}</label></div>
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="pictures[{{$i}}]"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
    @if ($i==5)
    <hr>
    @endif
    @endfor




   
    <!-- Nav tabs -->
<!--    <ul class="nav nav-tabs">
        @foreach ($languages as $index => $language)
        <?php
        $msg = json_decode($errors, true);
        isset($msg['lang_' . $language->id]) ? $error = 'error' : $error = '';
        ?>
        <li @if($index == 0) class="active" @endif><a id="{{$language->id}}" href="#{{$language->name}}" data-toggle="tab">{{ ($language->required)?$language->name.'*':$language->name}} @if($error == 'error') <span class="error glyphicon glyphicon-ban-circle"></span> @endif</a></li>
        @endforeach
    </ul>-->

    <input type="hidden" name="category_id" value="{{$category_id}}">
    <!-- Tab panes -->
    <div class="tab-content">
        @foreach ($languages as $index => $language)
        <div class="tab-pane @if($index == 0) active @endif" id="<?php echo $language->name; ?>"><br>
            <?php
            $msg = json_decode($errors, true);
            isset($msg['lang_' . $language->id]) ? $messages = $msg['lang_' . $language->id] : $messages = array();
            $input = Input::old('lang_' . $language->id);
            ?>
            <div class="form-group col-lg-12">
                <fieldset>
                    <input class="language" type="hidden" data-required="{{in_array($language->id,$requiredLanguageIds)}}" name="lang_{{$language->id}}[language_id]" value="{{$language->id}}">
                    <h2>Specs</h2>
                    <hr/>
                    @include('partials.specs', array('languageId' => $language->id, 'input' => $input))
                     <h2>Videos</h2>
                    <hr/>
                    @include('partials.videos', array('languageId' => $language->id, 'language' => $language->slug, 'messages' => $messages))
                </fieldset>
            </div>
        </div>
        @endforeach
    </div>

    <div class="form-submit col-md-2 col-md-offset-5">
        <button type="submit" class="btn btn-success pull-right">Save</button>
        <a class="btn btn-danger pull-right" href="/machine-managment/{{$category_id}}">Cancel</a>
    </div>
</form>
@stop