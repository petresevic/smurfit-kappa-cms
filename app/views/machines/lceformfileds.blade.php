
<div class="spec-lce-item">
 
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][model]">Model</label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][model]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][weight]">Machine weight(kg)</label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][weight]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][impact_force]">Impact force kN/blow </label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][impact_force]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][engine_rated_output]">Engine rated output kW (HP) </label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][engine_rated_output]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][engine_rated_output_ps]">Engine rated output kW (PS) </label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][engine_rated_output_ps]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    
    <div class="input-field-group">
        <div class="col-lg-3">
            <label class="control-label pull-right" for="machine[{{$randNumber}}][centrifugal_force]">Centrifugal force kN (kgf) </label>
        </div>
        <div class="col-lg-6">
            <input name="machine[{{$randNumber}}][centrifugal_force]" value="" type="text" class="form-control input-md">
        </div>
        <div class="error col-lg-3"></div>
    </div>
    
    <div class="input-field-group">
        <div class="col-lg-3"></div>
        <div class="col-lg-7"></div>
        <div class="error col-lg-2"><a class="delete-row btn btn-danger" href="javascript:void(0)" >Remove</a></div>
    </div>
    <hr>
</div>