@extends('layouts/main')

@section('content')

    {{$breadcrumbs}}

    <h1>Categories</h1>
    <ol>
    @foreach ($categories as $category)
        @if ($category->parent_id == null)
            @if (count($category->subCategories) > 0)
                <li>{{$category->title}}</li>
                <ol>
                    @foreach ($category->subCategories as $subCategory)
                        <li>
                            <a href="machine-managment/{{$subCategory->id}}">{{$subCategory->title}}</a>
                        </li>
                    @endforeach
                </ol>
            @else
                <li><a href="machine-managment/{{$category->id}}">{{$category->title}}</a></li>
            @endif
        @endif
    @endforeach
    </ol>
@stop