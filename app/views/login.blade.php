@extends('layouts/main')

@section('content')
        <div class="login-form col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign in</h3>
                </div>
                <?php if(isset($validator)) print_r($validator); ?>
                <div class="panel-body">
                    {{ Form::open(array('url' => 'login')) }}
                    <fieldset>
                        <p>
                            {{ $errors->first('username') }}
                        </p>
                        <div class="form-group">
                            {{ Form::text('username', Input::old('username'), array('placeholder' => 'Username', 'class' => 'form-control')) }}
                        </div>
                        <p>
                            {{ $errors->first('password') }}
                        </p>
                        <div class="form-group">
                            {{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control')) }}
                        </div>
                        {{ Form::submit('Login', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
@stop