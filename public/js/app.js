$(document).ready(function () {

    $('td, th', '#sortable').each(function () {
        var cell = $(this);
        cell.width(cell.width());
    });

    $("#sortable").sortable({
        axis: 'y',
        handle: '.handle',
        update: function (event, ui) {
            var data = $(this).sortable('serialize');

            var ids = [];

            $("#sortable tr").each(function () {
                ids.push($(this).data('id'));
            });

            $.post('/update-place', {ids: ids});

        }
    });

    $("#sortable").disableSelection();

    $('#add-machine, #edit-machine').submit(function (e) {

        var valid = true;
        var errorTabId = false;

        // clear old errors
        $('.error').html('');
        
        $("input[type='file']").each(function () {
            var $this = $(this);

            var inputName = $this.attr('name');
                      
            
            var mainPicture = inputName === 'main_picture';
            

            var parent = $this.closest('.fileinput');
            var preview = parent.find('.fileinput-preview');
            var fileinput = parent.find('.fileinput-new');

            // main and header photos
            if (mainPicture) {
                if (preview.html().trim() === '' && fileinput.html().trim() === '') {
                    parent.find('.error').html('This field is required.');
                    valid = false;
                }
            }

            
            

           
        });
        
         if(!$('#lang_1_name').val()) {
            $('.error_lang_1_name').html('Value is required.');
            valid = false;
        }
         
        if(!$('#lang_1_designer').val()) {
            $('.error_lang_1_designer').html('Value is required.');
            valid = false;
        }
        
        if(!$('#lang_1_country').val()) {
            $('.error_lang_1_country').html('Value is required.');
            valid = false;
        }
               
        
        if(!$('#lang_1_intro').val()) {
            
            $('.error_lang_1_intro').html('Value is required.');
            valid = false;
        }
        
        

        if (!(valid)) {
            $('#' + errorTabId).trigger('click');
            alert('There are some errors!');
        }
        
        return valid && photoRequired;
    });

    $('.remove-video').on('click', function (e) {
        e.preventDefault();
        var container = $(this).parent();
        var machine = $(this).data('machine');
        var language = $(this).data('language');
        var video = $(this).data('video');

        $.ajax({
            url: "/delete-video/" + machine + "/" + language + "/" + video
        }).done(function () {
            container.hide();
        });
    });

    $('.show-crop-modal').on('click', function () {
        var image = $(this).closest('.fileinput').find('.original-image').clone();
        console.log(image.data-image-name);
        image.removeClass('hidden');
        image.attr('id', 'target-image');

        $('.modal-body').html('');
        $('.modal-body').append(image);

        $('#target-image').Jcrop({
            boxWidth: 460,
            boxHeight: 620,
            setSelect: [586, 815, 0, 0],
            allowResize: true,
            allowSelect: false,
            onSelect: updateCoords
        });

        $('#cropImageModal').modal('show');
    });

    var coordX, coordY, width, height;

    function updateCoords(c) {
        coordX = c.x;
        coordY = c.y;
        width = c.w;
        height = c.h;
    }

    $('.crop-image').on('click', function () {
        var url = "/create-thumbnail";
        var image = $(this).closest('#cropImageModal').find('img');
        
        var machineId = image.data('machine-id');
        var imageName = image.data('image-name');

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'x': coordX,
                'y': coordY,
                'w': width,
                'h': height,
                'machineId': machineId,
                'imageName': imageName
            },
            success: function () {
                window.location.reload();
            }
        });
    });

    $('#cropImageModal').on('show.bs.modal shown.bs.modal', function () {
        $(this).find('.modal-dialog').css({
            width: 458 + 40,
            height: 612 + 40
        });
    });

    $('.remove-image').on('click', function () {
        var url = "/remove-image";
        var machineId = $(this).data('machine-id');
        var imageName = $(this).data('image-name');

        console.log(machineId, imageName);

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'machineId': machineId,
                'imageName': imageName
            },
            success: function () {
                window.location.reload();
            }
        });
    });

    $('.fileinput-preview').bind('DOMNodeInserted DOMNodeRemoved', function () {
        $(this).closest('.fileinput').find('.thumbnail-warning').css({
            visibility: 'visible'
        });
    });


});